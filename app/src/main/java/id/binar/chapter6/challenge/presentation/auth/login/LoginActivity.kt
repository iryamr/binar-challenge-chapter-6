package id.binar.chapter6.challenge.presentation.auth.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter6.challenge.databinding.ActivityLoginBinding
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.presentation.MainActivity
import id.binar.chapter6.challenge.presentation.auth.register.RegisterActivity
import id.binar.chapter6.challenge.utils.Extensions.isValidated

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        checkLogin()
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        doLogin()
        doRegister()
    }

    private fun checkLogin() {
        viewModel.email.observe(this) { email ->
            if (email.isNotEmpty()) {
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("email", email)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun doLogin() {
        binding.btnLogin.setOnClickListener {
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()

            if (validateData(email, password)) {
                viewModel.login(email, password)
                doObserveLogin()
            }
        }
    }

    private fun doRegister() {
        binding.tvRegister.setOnClickListener {
            Intent(this, RegisterActivity::class.java).also { startActivity(it) }
        }
    }

    private fun doObserveLogin() {
        viewModel.login.observe(this) { result ->
            when (result) {
                is Result.Loading -> {}
                is Result.Success -> {
                    viewModel.setLogin(result.data.email!!)
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                is Result.Error -> {
                    Toast.makeText(this, result.error, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun validateData(email: String, password: String): Boolean {
        return when {
            email.isEmpty() -> {
                binding.etlEmail.error = "Email tidak boleh kosong"
                binding.etlEmail.requestFocus()
                false
            }
            !email.isValidated() -> {
                binding.etlEmail.error = "Email tidak valid"
                binding.etlEmail.requestFocus()
                false
            }
            password.isEmpty() -> {
                binding.etlPassword.error = "Password tidak boleh kosong"
                binding.etlPassword.requestFocus()
                false
            }
            password.length < MIN_PASSWORD_LENGTH -> {
                binding.etlPassword.error =
                    "Password harus lebih dari $MIN_PASSWORD_LENGTH karakter"
                binding.etlPassword.requestFocus()
                false
            }
            else -> true
        }
    }

    companion object {
        private const val MIN_PASSWORD_LENGTH = 6
    }
}