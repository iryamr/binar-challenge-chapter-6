package id.binar.chapter6.challenge.presentation.auth.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter6.challenge.data.sources.local.UserPreference
import id.binar.chapter6.challenge.databinding.ActivityRegisterBinding
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.presentation.MainActivity
import id.binar.chapter6.challenge.utils.Extensions.isValidated
import javax.inject.Inject

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    @Inject
    lateinit var pref: UserPreference

    private val viewModel: RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        register()
    }

    private fun register() {
        binding.btnRegister.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val email = binding.etEmail.text.toString()
            val password = binding.etPassword.text.toString()
            val passwordConfirm = binding.etPasswordConfirm.text.toString()

            if (validateData(username, email, password, passwordConfirm)) {
                viewModel.register(username, email, password)
                doObserveRegister(email)
            }
        }
    }

    private fun doObserveRegister(email: String) {
        viewModel.register.observe(this) { result ->
            when (result) {
                is Result.Loading -> {}
                is Result.Success -> {
                    pref.login(email)
                    Toast.makeText(this, "User created", Toast.LENGTH_SHORT).show()
                    moveToMainActivity()
                }
                is Result.Error -> {
                    Toast.makeText(this, result.error, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun validateData(
        username: String,
        email: String,
        password: String,
        passwordConfirm: String
    ): Boolean {
        return when {
            username.isEmpty() -> {
                binding.etlUsername.error = "Username tidak boleh kosong"
                binding.etlUsername.requestFocus()
                false
            }
            email.isEmpty() -> {
                binding.etlEmail.error = "Email tidak boleh kosong"
                binding.etlEmail.requestFocus()
                false
            }
            !email.isValidated() -> {
                binding.etlEmail.error = "Email tidak valid"
                binding.etlEmail.requestFocus()
                false
            }
            password.isEmpty() -> {
                binding.etlPassword.error = "Password tidak boleh kosong"
                binding.etlPassword.requestFocus()
                false
            }
            password.length < MIN_PASSWORD_LENGTH -> {
                binding.etlPassword.error =
                    "Password harus lebih dari $MIN_PASSWORD_LENGTH karakter"
                binding.etlPassword.requestFocus()
                false
            }
            passwordConfirm.isEmpty() -> {
                binding.etlPasswordConfirm.error = "Konfirmasi password tidak boleh kosong"
                binding.etlPasswordConfirm.requestFocus()
                false
            }
            passwordConfirm != password -> {
                binding.etlPasswordConfirm.error = "Password tidak sesuai"
                binding.etlPasswordConfirm.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun moveToMainActivity() {
        Intent(this, MainActivity::class.java).also { intent ->
            startActivity(intent)
            finish()
        }
    }

    private fun backToLogin() {
        binding.tvLogin.setOnClickListener { onBackPressed() }
    }

    companion object {
        private const val MIN_PASSWORD_LENGTH = 6
    }
}