package id.binar.chapter6.challenge.presentation.movie.movie_detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.domain.repositories.MovieRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    private val _movieDetails = MutableLiveData<Result<Movie>>(Result.Loading)
    val movieDetails: LiveData<Result<Movie>> = _movieDetails

    private val _movieCasts = MutableLiveData<Result<List<CastDetail>>>(Result.Loading)
    val movieCasts: LiveData<Result<List<CastDetail>>> = _movieCasts

    private val _favoriteMovie = MutableLiveData<FavoriteMovie?>()
    val favoriteMovie: LiveData<FavoriteMovie?> = _favoriteMovie

    fun getMovieDetails(movieId: Int) {
        viewModelScope.launch {
            val data = async { repository.getMovieDetail(movieId) }
            val results = data.await()
            _movieDetails.postValue(results)
        }
    }

    fun getMovieCasts(movieId: Int) {
        viewModelScope.launch {
            val data = async { repository.getMovieCasts(movieId) }
            val results = data.await()
            _movieCasts.postValue(results)
        }
    }

    fun getFavoriteMovie(userId: Int, movieId: Int) {
        viewModelScope.launch {
            val data = async { repository.getFavoriteMovie(userId, movieId) }
            val result = data.await()
            _favoriteMovie.postValue(result)
        }
    }

    fun addToFavorite(movie: Movie) {
        viewModelScope.launch {
            repository.addToFavorite(movie)
        }
    }

    fun removeFromFavorite(userId: Int, movieId: Int?) {
        viewModelScope.launch {
            repository.removeFromFavorite(userId, movieId)
        }
    }
}