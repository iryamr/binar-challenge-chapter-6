package id.binar.chapter6.challenge.domain.repositories

import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.domain.models.movie.MovieDetail

interface MovieDetailRepository {

    suspend fun getMovieDetail(movieId: Int): MovieDetail

    suspend fun getMovieCasts(movieId: Int): List<CastDetail>

//    suspend fun getSimiliarMovies(movieId: Int): List<Movie>
}