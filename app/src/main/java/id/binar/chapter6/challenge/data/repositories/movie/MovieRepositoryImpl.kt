package id.binar.chapter6.challenge.data.repositories.movie

import id.binar.chapter6.challenge.data.mappers.toDomain
import id.binar.chapter6.challenge.data.mappers.toEntity
import id.binar.chapter6.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter6.challenge.data.sources.remote.service.ApiService
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.domain.repositories.MovieRepository
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val movieDao: MovieDao
) : MovieRepository {

    override suspend fun getNowPlayingMovies(): Result<List<Movie>> {
        return try {
            val response = apiService.fetchNowPlayingMovies()
            val movieEntity = response.results.map { it.toEntity() }
            movieDao.insertMovies(movieEntity)

            val data = movieDao.getNowPlayingMovies().map { it.toDomain() }
            Result.Success(data)
        } catch (e: NullPointerException) {
            Result.Error("Data not found")
        } catch (e: IOException) {
            Result.Error("Something is wrong")
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        }
    }

    override suspend fun getPopularMovies(): Result<List<Movie>> {
        return try {
            val response = apiService.fetchPopularMovies()
            val movieEntity = response.results.map { it.toEntity() }
            movieDao.insertMovies(movieEntity)

            val data = movieDao.getPopularMovies().map { it.toDomain() }
            Result.Success(data)
        } catch (e: NullPointerException) {
            Result.Error("Data not found")
        } catch (e: IOException) {
            Result.Error("Something is wrong")
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        }
    }

    override suspend fun getFavoriteMovies(userId: Int): Result<List<FavoriteMovie>> {
        return try {
            val data = movieDao.getFavoriteMovies(userId).map { it.toDomain() }
            Result.Success(data)
        } catch (e: NullPointerException) {
            Result.Error("Favorite movies not found")
        } catch (e: IOException) {
            Result.Error("Something is wrong")
        }
    }

    override suspend fun getFavoriteMovie(userId: Int, movieId: Int): FavoriteMovie? {
        return movieDao.getFavoriteMovie(userId, movieId)?.toDomain()
    }

    override suspend fun getMovieDetail(movieId: Int): Result<Movie> {
        return try {
            val response = apiService.fetchMovieDetails(movieId).toDomain()
            Result.Success(response)
        } catch (e: NullPointerException) {
            Result.Error("Movie not found")
        } catch (e: IOException) {
            Result.Error("Something is wrong")
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        }
    }

    override suspend fun getMovieCasts(movieId: Int): Result<List<CastDetail>> {
        return try {
            val response = apiService.fetchMovieCast(movieId)
            val data = response.cast.map { it.toDomain() }
            Result.Success(data)
        } catch (e: NullPointerException) {
            Result.Error("Data not found")
        } catch (e: IOException) {
            Result.Error("Something is wrong")
        } catch (e: HttpException) {
            Result.Error("Please check your connection")
        }
    }

    override suspend fun addToFavorite(movie: Movie) {
        movieDao.insertFavoriteMovie(movie.toEntity())
    }

    override suspend fun removeFromFavorite(userId: Int, movieId: Int?) {
        movieDao.deleteFavoriteMovie(userId, movieId)
    }
}