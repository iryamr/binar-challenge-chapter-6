package id.binar.chapter6.challenge.presentation.auth.login

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.data.sources.local.UserPreference
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val pref: UserPreference
) : ViewModel() {

    private val _login = MutableLiveData<Result<User>>(Result.Loading)
    val login: LiveData<Result<User>> = _login

    val email = pref.getLoggedInEmail().asLiveData()

    fun login(email: String, password: String) {
        viewModelScope.launch {
            val data = async { repository.login(email, password) }
            val result = data.await()
            _login.value = result
        }
    }

    fun setLogin(email: String) {
        pref.login(email)
    }
}