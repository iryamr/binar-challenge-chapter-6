package id.binar.chapter6.challenge.data.sources.remote.responses

import com.google.gson.annotations.SerializedName

data class CastDto(

    @SerializedName("id")
    val id: Int,

    @SerializedName("cast")
    val cast: List<CastDetailDto>
)
