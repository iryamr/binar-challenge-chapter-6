package id.binar.chapter6.challenge.domain.repositories

import id.binar.chapter6.challenge.data.sources.local.entities.user.UserAndProfile
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.Profile
import id.binar.chapter6.challenge.domain.models.auth.User

interface AuthRepository {

    suspend fun login(email: String, password: String): Result<User>

    suspend fun register(user: User): Result<Long>

    suspend fun checkEmail(email: String): Boolean

    suspend fun checkCredentials(email: String?, password: String?): Boolean

    suspend fun getUser(email: String): Result<User>

    suspend fun updateUser(user: User): Result<Int>

    suspend fun getProfile(userId: Int): Result<UserAndProfile>

    suspend fun createProfile(profile: Profile): Result<Long>

    suspend fun updateProfile(profile: Profile): Result<Int>
}