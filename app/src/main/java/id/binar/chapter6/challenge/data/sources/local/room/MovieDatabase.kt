package id.binar.chapter6.challenge.data.sources.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import id.binar.chapter6.challenge.data.sources.local.entities.movie.FavoriteMovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.movie.MovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.ProfileEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserEntity
import id.binar.chapter6.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter6.challenge.data.sources.local.room.dao.UserDao

@Database(
    entities = [
        UserEntity::class,
        ProfileEntity::class,
        MovieEntity::class,
        FavoriteMovieEntity::class,
//        NowPlayingMovieEntity::class,
//        PopularMovieEntity::class,
//        SimiliarMovieEntity::class,
    ],
    version = 1,
    exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun movieDao(): MovieDao
}