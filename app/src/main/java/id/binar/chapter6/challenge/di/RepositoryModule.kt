package id.binar.chapter6.challenge.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.binar.chapter6.challenge.data.repositories.auth.AuthRepositoryImpl
import id.binar.chapter6.challenge.data.repositories.movie.MovieRepositoryImpl
import id.binar.chapter6.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter6.challenge.data.sources.local.room.dao.UserDao
import id.binar.chapter6.challenge.data.sources.remote.service.ApiService
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import id.binar.chapter6.challenge.domain.repositories.MovieRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(apiService: ApiService, movieDao: MovieDao) =
        MovieRepositoryImpl(apiService, movieDao) as MovieRepository

    @Provides
    @Singleton
    fun provideAuthRepository(userDao: UserDao) =
        AuthRepositoryImpl(userDao) as AuthRepository
}