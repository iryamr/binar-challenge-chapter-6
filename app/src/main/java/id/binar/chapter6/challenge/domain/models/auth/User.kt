package id.binar.chapter6.challenge.domain.models.auth

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    val id: Int = 0,
    var username: String? = null,
    val email: String? = null,
    val password: String? = null
) : Parcelable
