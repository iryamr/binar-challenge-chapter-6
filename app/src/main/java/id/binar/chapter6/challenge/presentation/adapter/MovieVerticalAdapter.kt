package id.binar.chapter6.challenge.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.binar.chapter6.challenge.databinding.ItemMoviesVerticalBinding
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.utils.Extensions.loadImage
import id.binar.chapter6.challenge.utils.Extensions.toRating

class MovieVerticalAdapter(
    private val onClick: (Movie) -> Unit
) : ListAdapter<Movie, MovieVerticalAdapter.MovieViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            ItemMoviesVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class MovieViewHolder(private val binding: ItemMoviesVerticalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie) {
            with(binding) {
                tvTitle.text = movie.title
                tvOverview.text = movie.overview
                ratingBar.rating = movie.voteAverage.toRating()
                ivPoster.loadImage(movie.posterPath)
                cvPoster.setOnClickListener { onClick(movie) }
                root.setOnClickListener { onClick(movie) }
            }
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }
}