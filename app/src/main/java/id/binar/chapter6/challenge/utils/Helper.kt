package id.binar.chapter6.challenge.utils

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.widget.Toast
import id.binar.chapter6.challenge.R

object Helper {

    fun setTitleWithYear(context: Context, title: String, year: String?): SpannableStringBuilder {
        val text = context.getString(R.string.title, title, year)

        val titleStart = text.indexOf(title)
        val titleEnd = titleStart + title.length

        val yearColor = Color.GRAY
        val yearStart = text.indexOf(year!!)
        val yearEnd = yearStart + year.length

        return SpannableStringBuilder(text).apply {
            setSpan(
                StyleSpan(Typeface.BOLD),
                titleStart,
                titleEnd,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(yearColor),
                yearStart,
                yearEnd,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}