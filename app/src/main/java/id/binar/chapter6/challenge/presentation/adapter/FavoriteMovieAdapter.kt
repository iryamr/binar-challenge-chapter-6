package id.binar.chapter6.challenge.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.binar.chapter6.challenge.databinding.ItemMoviesVerticalBinding
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.utils.Extensions.loadImage
import id.binar.chapter6.challenge.utils.Extensions.toRating

class FavoriteMovieAdapter(
    private val onClick: (FavoriteMovie) -> Unit
) : ListAdapter<FavoriteMovie, FavoriteMovieAdapter.MovieViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            ItemMoviesVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class MovieViewHolder(private val binding: ItemMoviesVerticalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var movie: FavoriteMovie

        fun bind(movie: FavoriteMovie) {
            this.movie = movie
            with(binding) {
                tvTitle.text = movie.title
                tvOverview.text = movie.overview
                ratingBar.rating = movie.voteAverage.toRating()
                ivPoster.loadImage(movie.posterPath)
                cvPoster.setOnClickListener { onClick(movie) }
                root.setOnClickListener { onClick(movie) }
            }
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<FavoriteMovie>() {
            override fun areItemsTheSame(oldItem: FavoriteMovie, newItem: FavoriteMovie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: FavoriteMovie,
                newItem: FavoriteMovie
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}