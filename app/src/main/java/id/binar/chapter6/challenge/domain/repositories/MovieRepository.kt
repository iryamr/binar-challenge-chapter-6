package id.binar.chapter6.challenge.domain.repositories

import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.models.movie.Movie

interface MovieRepository {

    suspend fun getNowPlayingMovies(): Result<List<Movie>>

    suspend fun getPopularMovies(): Result<List<Movie>>

    suspend fun getFavoriteMovies(userId: Int): Result<List<FavoriteMovie>>

    suspend fun getFavoriteMovie(userId: Int, movieId: Int): FavoriteMovie?

    suspend fun getMovieDetail(movieId: Int): Result<Movie>

    suspend fun getMovieCasts(movieId: Int): Result<List<CastDetail>>

    suspend fun addToFavorite(movie: Movie)

    suspend fun removeFromFavorite(userId: Int, movieId: Int?)
}