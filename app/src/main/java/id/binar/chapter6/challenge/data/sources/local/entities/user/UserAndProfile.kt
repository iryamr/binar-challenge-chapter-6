package id.binar.chapter6.challenge.data.sources.local.entities.user

import androidx.room.Embedded
import androidx.room.Relation

data class UserAndProfile(
    @Embedded
    val user: UserEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "user_id"
    )
    var profile: ProfileEntity? = null
)
