package id.binar.chapter6.challenge.domain.models.movie

data class Cast(
    val cast: List<CastDetail>,
    val id: Int
)
