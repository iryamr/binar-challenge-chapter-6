package id.binar.chapter6.challenge.data.sources.local.room.dao

import androidx.room.*
import id.binar.chapter6.challenge.data.sources.local.entities.user.ProfileEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserAndProfile
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserEntity

@Dao
interface UserDao {
    @Query("SELECT * FROM users WHERE email = :email AND password = :password")
    suspend fun login(email: String?, password: String?): UserEntity

    @Insert
    suspend fun register(user: UserEntity): Long

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email)")
    suspend fun checkEmail(email: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM users WHERE email = :email AND password = :password LIMIT 1)")
    suspend fun checkCredentials(email: String?, password: String?): Boolean

    @Query("SELECT * FROM users WHERE email = :email LIMIT 1")
    suspend fun getUser(email: String): UserEntity

    @Update
    suspend fun updateUser(user: UserEntity): Int

    @Transaction
    @Query("SELECT * FROM users WHERE id = :userId LIMIT 1")
    suspend fun getProfile(userId: Int): UserAndProfile

    @Insert
    suspend fun insertProfile(profile: ProfileEntity): Long

    @Update
    suspend fun updateProfile(profile: ProfileEntity): Int
}