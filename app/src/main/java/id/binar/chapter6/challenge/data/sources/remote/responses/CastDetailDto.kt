package id.binar.chapter6.challenge.data.sources.remote.responses

import com.google.gson.annotations.SerializedName

data class CastDetailDto(

    @SerializedName("id")
    val id: Int,

    @SerializedName("cast_id")
    val castId: Int,

    val movieId: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("original_name")
    val originalName: String,

    @SerializedName("character")
    val character: String,

    @SerializedName("profile_path")
    val profilePath: String
)
