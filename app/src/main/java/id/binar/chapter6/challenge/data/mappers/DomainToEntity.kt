package id.binar.chapter6.challenge.data.mappers

import id.binar.chapter6.challenge.data.sources.local.entities.movie.FavoriteMovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.ProfileEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserEntity
import id.binar.chapter6.challenge.domain.models.auth.Profile
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.models.movie.Movie

fun User.toEntity(): UserEntity =
    UserEntity(
        id = id,
        username = username,
        email = email,
        password = password,
    )

fun Profile.toEntity(): ProfileEntity =
    ProfileEntity(
        id = id,
        userId = userId,
        name = name,
        address = address,
        dateOfBirth = dateOfBirth
    )

fun Movie.toEntity(): FavoriteMovieEntity =
    FavoriteMovieEntity(
        id = id,
        userId = userId,
        movieId = id,
        title = title,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        popularity = popularity,
        voteAverage = voteAverage,
    )