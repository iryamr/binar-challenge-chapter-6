package id.binar.chapter6.challenge.domain.models.movie

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
    val id: Int,
    val movieId: Int = 0,
    var userId: Int = 0,
    val title: String,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String,
    val genres: List<Genre> = listOf(),
    val overview: String,
    val popularity: Double = 0.0,
    val voteAverage: Double = 0.0,
) : Parcelable