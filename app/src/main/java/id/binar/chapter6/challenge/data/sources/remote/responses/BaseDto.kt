package id.binar.chapter6.challenge.data.sources.remote.responses

import com.google.gson.annotations.SerializedName

data class BaseDto<out T>(

    @SerializedName("results")
    val results: List<T>
)
