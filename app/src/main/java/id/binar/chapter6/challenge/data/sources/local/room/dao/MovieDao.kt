package id.binar.chapter6.challenge.data.sources.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import id.binar.chapter6.challenge.data.sources.local.entities.movie.FavoriteMovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.movie.MovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movies WHERE release_date BETWEEN '2022-03-01' AND '2022-05-19' ORDER BY release_date DESC")
    suspend fun getNowPlayingMovies(): List<MovieEntity>

    @Query("SELECT * FROM movies ORDER BY popularity DESC")
    suspend fun getPopularMovies(): List<MovieEntity>

    @Query("SELECT * FROM favorite_movies WHERE user_id = :userId")
    suspend fun getFavoriteMovies(userId: Int): List<FavoriteMovieEntity>

    @Query("SELECT * FROM favorite_movies WHERE user_id = :userId AND movie_id = :movieId")
    suspend fun getFavoriteMovie(userId: Int, movieId: Int): FavoriteMovieEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(movies: List<MovieEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteMovie(movie: FavoriteMovieEntity)

    @Query("DELETE FROM movies")
    suspend fun deleteMovies()

    @Query("DELETE FROM favorite_movies WHERE user_id = :userId AND movie_id = :movieId")
    suspend fun deleteFavoriteMovie(userId: Int, movieId: Int?)
}