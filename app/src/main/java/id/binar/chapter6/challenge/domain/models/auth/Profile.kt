package id.binar.chapter6.challenge.domain.models.auth

data class Profile(
    var id: Int = 0,
    var userId: Int = 0,
    var name: String? = null,
    var address: String? = null,
    var dateOfBirth: String? = null,
)
