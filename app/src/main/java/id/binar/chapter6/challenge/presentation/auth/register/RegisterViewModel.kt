package id.binar.chapter6.challenge.presentation.auth.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repository: AuthRepository
) : ViewModel() {

    private val _register = MutableLiveData<Result<Long>>()
    val register: LiveData<Result<Long>> = _register

    fun register(username: String, email: String, password: String) {
        viewModelScope.launch {
            val user = User(
                username = username,
                email = email,
                password = password,
            )
            _register.value = repository.register(user)
        }
    }
}