package id.binar.chapter6.challenge.presentation.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter6.challenge.databinding.FragmentFavoriteBinding
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.presentation.adapter.FavoriteMovieAdapter
import id.binar.chapter6.challenge.utils.Helper.showToast

@AndroidEntryPoint
class FavoriteFragment : Fragment() {

    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FavoriteViewModel by viewModels()
    private val args: FavoriteFragmentArgs by navArgs()

    private val userId: Int by lazy { args.userId }

    private val movieAdapter: FavoriteMovieAdapter by lazy { FavoriteMovieAdapter(::onMovieClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoriteBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doObserveFavoriteMovies()
//        initSwipeAction()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun doObserveFavoriteMovies() {
        viewModel.getFavoriteMovies(userId)
        viewModel.movies.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                }
                is Result.Success -> {
                    initFavoriteMovies(result.data)
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                }
            }
        }
    }

    private fun initFavoriteMovies(movies: List<FavoriteMovie>) {
        if (movies.isNullOrEmpty()) {
            showEmptyState("Favorite movies is empty")
        } else {
            movieAdapter.submitList(movies)
            binding.rvFavoriteMovies.adapter = movieAdapter
            binding.rvFavoriteMovies.layoutManager = initLayoutManager(VERTICAL)
        }
    }

    private fun initLayoutManager(oritentation: Int): LinearLayoutManager {
        return LinearLayoutManager(requireContext(), oritentation, false)
    }

    private fun initSwipeAction() {
        val itemTouchHelper =
            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val movie = (viewHolder as FavoriteMovieAdapter.MovieViewHolder).movie
                    viewModel.removeFromFavorite(args.userId, movie.movieId)
                }
            })

        itemTouchHelper.attachToRecyclerView(binding.rvFavoriteMovies)
    }

    private fun showEmptyState(message: String) {
        binding.error.tvError.text = message
        binding.error.root.isVisible = true
        binding.rvFavoriteMovies.isVisible = false
    }

    private fun onMovieClicked(movie: FavoriteMovie) {
        val directions = FavoriteFragmentDirections.actionFavoriteFragmentToMovieDetailFragment(
            Movie(
                id = movie.movieId,
                userId = userId,
                movieId = movie.movieId,
                title = movie.title,
                posterPath = movie.posterPath,
                backdropPath = movie.backdropPath,
                releaseDate = movie.releaseDate,
                genres = listOf(),
                overview = movie.overview,
                popularity = movie.popularity,
                voteAverage = movie.voteAverage,
            ),
            userId
        )

        findNavController().navigate(directions)
    }

    companion object {
        private const val HORIZONTAL = LinearLayoutManager.HORIZONTAL
        private const val VERTICAL = LinearLayoutManager.VERTICAL
    }
}