package id.binar.chapter6.challenge.presentation.movie.movie_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.ViewCompat
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter6.challenge.R
import id.binar.chapter6.challenge.databinding.FragmentMovieBinding
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.presentation.adapter.MovieHorizontalAdapter
import id.binar.chapter6.challenge.presentation.adapter.MovieVerticalAdapter

@AndroidEntryPoint
class MovieFragment : Fragment() {

    private var _binding: FragmentMovieBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels()
    private val args: MovieFragmentArgs by navArgs()

    private var userId: Int = 0

    private val nowPlayingMovieAdapter: MovieHorizontalAdapter by lazy { MovieHorizontalAdapter(::onMovieClicked) }
    private val popularMovieAdapter: MovieVerticalAdapter by lazy { MovieVerticalAdapter(::onMovieClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doObserveUser()
        doObserveNowPlayingMovies()
        doObservePopularMovies()

        ViewCompat.getWindowInsetsController(requireActivity().window.decorView)?.isAppearanceLightStatusBars =
            true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun doObserveUser() {
        val email = args.email
        viewModel.getLoggedInUser(email)
        viewModel.user.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerUser()
                }
                is Result.Success -> {
                    hideShimmerUser()
                    userId = result.data.id
                    binding.content.tvGreetingHome.text =
                        getString(R.string.greeting, result.data.username)
                    onProfileClicked(userId)
                    onFavoriteClicked(userId)
                }
                is Result.Error -> {
                    hideShimmerUser()
                }
            }
        }
    }

    private fun doObserveNowPlayingMovies() {
        viewModel.nowPlayingMovies.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerHorizontal()
                }
                is Result.Success -> {
                    hideShimmerHorizontal()
                    initNowPlayingMovies(result.data)
                }
                is Result.Error -> {
                    hideShimmerHorizontal()
                }
            }
        }
    }

    private fun doObservePopularMovies() {
        viewModel.popularMovies.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerVertical()
                }
                is Result.Success -> {
                    hideShimmerVertical()
                    initPopularMovies(result.data)
                }
                is Result.Error -> {
                    hideShimmerVertical()
                }
            }
        }
    }

    private fun initNowPlayingMovies(movies: List<Movie>) {
        if (movies.isNullOrEmpty()) {

        } else {
            nowPlayingMovieAdapter.submitList(movies)
            binding.content.rvNowPlayingMovies.adapter = nowPlayingMovieAdapter
            binding.content.rvNowPlayingMovies.layoutManager = initLayoutManager(HORIZONTAL)
        }
    }

    private fun initPopularMovies(movies: List<Movie>) {
        if (movies.isNullOrEmpty()) {

        } else {
            popularMovieAdapter.submitList(movies)
            binding.content.rvPopularMovies.adapter = popularMovieAdapter
            binding.content.rvPopularMovies.layoutManager = initLayoutManager(VERTICAL)
        }
    }

    private fun initLayoutManager(oritentation: Int): LinearLayoutManager {
        return LinearLayoutManager(requireContext(), oritentation, false)
    }

    private fun onMovieClicked(movie: Movie) {
        Toast.makeText(requireContext(), movie.title, Toast.LENGTH_SHORT).show()
        val directions =
            MovieFragmentDirections.actionMovieFragmentToMovieDetailFragment(movie, userId)
        findNavController().navigate(directions)
    }

    private fun onProfileClicked(userId: Int) {
        binding.content.ivProfile.setOnClickListener {
            val directions = MovieFragmentDirections.actionMovieFragmentToProfileFragment(userId)
            findNavController().navigate(directions)
        }
    }

    private fun onFavoriteClicked(userId: Int) {
        binding.content.ivFavorite.setOnClickListener {
            val directions = MovieFragmentDirections.actionMovieFragmentToFavoriteFragment(userId)
            findNavController().navigate(directions)
        }
    }

    private fun showShimmerUser() {
        binding.apply {
            shimmer.shimmerUser.startShimmer()
            content.tvGreetingHome.isInvisible = true
        }
    }

    private fun hideShimmerUser() {
        binding.apply {
            shimmer.shimmerUser.isInvisible = true
            shimmer.shimmerUser.stopShimmer()
            content.tvGreetingHome.isInvisible = false
        }
    }

    private fun showShimmerHorizontal() {
        binding.apply {
            shimmer.shimmerHorizontal.startShimmer()
            content.rvNowPlayingMovies.isInvisible = true
        }
    }

    private fun hideShimmerHorizontal() {
        binding.apply {
            shimmer.shimmerHorizontal.isInvisible = true
            shimmer.shimmerHorizontal.stopShimmer()
            content.rvNowPlayingMovies.isInvisible = false
        }
    }

    private fun showShimmerVertical() {
        binding.apply {
            shimmer.shimmerHorizontal.startShimmer()
            content.rvPopularMovies.isInvisible = true
        }
    }

    private fun hideShimmerVertical() {
        binding.apply {
            shimmer.shimmerVertical.isInvisible = true
            shimmer.shimmerVertical.stopShimmer()
            content.rvPopularMovies.isInvisible = false
        }
    }

    companion object {
        private const val HORIZONTAL = LinearLayoutManager.HORIZONTAL
        private const val VERTICAL = LinearLayoutManager.VERTICAL
    }
}