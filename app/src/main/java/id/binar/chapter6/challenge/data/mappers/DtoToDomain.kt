package id.binar.chapter6.challenge.data.mappers

import id.binar.chapter6.challenge.data.sources.remote.responses.CastDetailDto
import id.binar.chapter6.challenge.data.sources.remote.responses.GenreDto
import id.binar.chapter6.challenge.data.sources.remote.responses.MovieDto
import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.domain.models.movie.Genre
import id.binar.chapter6.challenge.domain.models.movie.Movie

fun MovieDto.toDomain(): Movie {
    val genres = genres.map { it.toDomain() }

    return Movie(
        id = id,
        title = title,
        genres = genres,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        voteAverage = voteAverage,
    )
}

fun GenreDto.toDomain(): Genre =
    Genre(
        id = id,
        name = name
    )

fun CastDetailDto.toDomain(): CastDetail =
    CastDetail(
        id = id,
        movieId = movieId,
        castId = castId,
        name = name,
        originalName = originalName,
        character = character,
        profilePath = profilePath
    )