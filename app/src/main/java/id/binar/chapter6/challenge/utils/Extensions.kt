package id.binar.chapter6.challenge.utils

import android.graphics.Bitmap
import android.net.Uri
import android.util.Patterns
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.imageview.ShapeableImageView
import id.binar.chapter6.challenge.BuildConfig
import id.binar.chapter6.challenge.R
import java.text.SimpleDateFormat
import java.util.*

object Extensions {

    fun ShapeableImageView.loadImage(image: String?) {
        Glide.with(this.context)
            .load(BuildConfig.IMG_URL + image)
            .apply(
                RequestOptions
                    .placeholderOf(R.drawable.ic_loading)
                    .error(R.drawable.ic_error_image)
            )
            .into(this)
    }

    fun ShapeableImageView.loadImage(uri: Uri) {
        Glide.with(this.context)
            .load(uri)
            .apply(
                RequestOptions
                    .placeholderOf(R.drawable.ic_loading)
                    .error(R.drawable.ic_error_image)
            )
            .into(this)
    }

    fun ShapeableImageView.loadImage(bitmap: Bitmap) {
        Glide.with(this.context)
            .load(bitmap)
            .apply(
                RequestOptions
                    .placeholderOf(R.drawable.ic_loading)
                    .error(R.drawable.ic_error_image)
            )
            .into(this)
    }

    fun String.isValidated(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    fun String.toDate(): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "yyyy"

        val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())

        val inputDate = inputFormat.parse(this)

        return if (this != "") {
            inputDate?.let {
                "(${outputFormat.format(it)})"
            }
        } else {
            "-"
        }
    }

    fun Double.toRating(): Float {
        val rating = (this / 2).toFloat()
        return if (rating > 0.5f && rating < 1.0f) {
            1.5f
        } else if (rating > 1.5f && rating < 2.0f) {
            2.5f
        } else if (rating > 2.5f && rating < 3.0f) {
            2.5f
        } else if (rating > 3.5f && rating < 4.0f) {
            3.5f
        } else if (rating > 4.5f && rating < 5.0f) {
            4.5f
        } else {
            rating
        }
    }
}