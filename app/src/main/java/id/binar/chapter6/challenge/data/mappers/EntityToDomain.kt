package id.binar.chapter6.challenge.data.mappers

import id.binar.chapter6.challenge.data.sources.local.entities.movie.FavoriteMovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.movie.MovieEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.ProfileEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserEntity
import id.binar.chapter6.challenge.domain.models.auth.Profile
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.models.movie.Movie

fun MovieEntity.toDomain(): Movie =
    Movie(
        id = id,
        movieId = id,
        title = title,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        popularity = popularity,
        voteAverage = voteAverage,
    )

fun FavoriteMovieEntity.toDomain(): FavoriteMovie =
    FavoriteMovie(
        id = id,
        userId = userId,
        movieId = id,
        title = title,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        popularity = popularity,
        voteAverage = voteAverage,
    )

fun UserEntity.toDomain(): User =
    User(
        id = id,
        username = username,
        email = email,
        password = password,
    )

fun ProfileEntity.toDomain(): Profile =
    Profile(
        id = id,
        userId = userId,
        address = address,
        dateOfBirth = dateOfBirth
    )