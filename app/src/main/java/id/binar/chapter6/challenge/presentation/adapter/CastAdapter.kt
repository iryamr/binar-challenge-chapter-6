package id.binar.chapter6.challenge.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.binar.chapter6.challenge.databinding.ItemMoviesCastsBinding
import id.binar.chapter6.challenge.domain.models.movie.CastDetail
import id.binar.chapter6.challenge.utils.Extensions.loadImage

class CastAdapter : ListAdapter<CastDetail, CastAdapter.CastViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
        val binding =
            ItemMoviesCastsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class CastViewHolder(private val binding: ItemMoviesCastsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(cast: CastDetail) {
            with(binding) {
                tvCastName.text = cast.name
                tvCastCharacter.text = cast.character
                ivCastImage.loadImage(cast.profilePath)
            }
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CastDetail>() {
            override fun areItemsTheSame(oldItem: CastDetail, newItem: CastDetail): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CastDetail, newItem: CastDetail): Boolean {
                return oldItem == newItem
            }
        }
    }
}