package id.binar.chapter6.challenge.data.repositories.auth

import id.binar.chapter6.challenge.data.mappers.toDomain
import id.binar.chapter6.challenge.data.mappers.toEntity
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserAndProfile
import id.binar.chapter6.challenge.data.sources.local.room.dao.UserDao
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.Profile
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
) : AuthRepository {

    override suspend fun login(email: String, password: String): Result<User> {
        return try {
            val data = userDao.login(email, password)
            Result.Success(data.toDomain())
        } catch (e: NullPointerException) {
            Result.Error("User not found")
        }
    }

    override suspend fun register(user: User): Result<Long> {
        return try {
            val userEntity = user.toEntity()
            val result = userDao.register(userEntity)

            val login = userDao.login(user.email, user.password)
            createProfile(Profile(userId = login.id))

            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Sign up failed")
        }
    }

    override suspend fun checkEmail(email: String): Boolean =
        userDao.checkEmail(email)

    override suspend fun checkCredentials(email: String?, password: String?): Boolean =
        userDao.checkCredentials(email, password)

    override suspend fun getUser(email: String): Result<User> {
        return try {
            val data = userDao.getUser(email)
            Result.Success(data.toDomain())
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "")
        }
    }

    override suspend fun updateUser(user: User): Result<Int> {
        return try {
            val userEntity = user.toEntity()
            val result = userDao.updateUser(userEntity)
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Update user failed")
        }
    }

    override suspend fun getProfile(userId: Int): Result<UserAndProfile> {
        return try {
            val result = userDao.getProfile(userId)
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "User not found")
        }
    }

    override suspend fun createProfile(profile: Profile): Result<Long> {
        return try {
            val profileEntity = profile.toEntity()
            val result = userDao.insertProfile(profileEntity)
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Sign up failed")
        }
    }

    override suspend fun updateProfile(profile: Profile): Result<Int> {
        return try {
            val profileEntity = profile.toEntity()
            val result = userDao.updateProfile(profileEntity)
            Result.Success(result)
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Update profile failed")
        }
    }
}