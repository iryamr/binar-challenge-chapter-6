package id.binar.chapter6.challenge.presentation.favorite

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter6.challenge.domain.repositories.MovieRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    private val _movies = MutableLiveData<Result<List<FavoriteMovie>>>(Result.Loading)
    val movies: LiveData<Result<List<FavoriteMovie>>> = _movies

    fun getFavoriteMovies(userId: Int) {
        viewModelScope.launch {
            val data = async { repository.getFavoriteMovies(userId) }
            val result = data.await()
            _movies.postValue(result)
        }
    }

    fun removeFromFavorite(userId: Int, movieId: Int) {
        viewModelScope.launch {
            repository.removeFromFavorite(userId, movieId)
        }
    }
}