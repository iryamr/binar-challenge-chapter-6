package id.binar.chapter6.challenge.data.sources.remote.service

import id.binar.chapter6.challenge.data.sources.remote.responses.BaseDto
import id.binar.chapter6.challenge.data.sources.remote.responses.CastDto
import id.binar.chapter6.challenge.data.sources.remote.responses.MovieDto
import id.binar.chapter6.challenge.utils.Endpoints
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET(Endpoints.NOW_PLAYING_MOVIES)
    suspend fun fetchNowPlayingMovies(): BaseDto<MovieDto>

    @GET(Endpoints.POPULAR_MOVIES)
    suspend fun fetchPopularMovies(): BaseDto<MovieDto>

    @GET(Endpoints.TOP_RATED_MOVIES)
    suspend fun fetchTopRatedMovies(): BaseDto<MovieDto>

    @GET(Endpoints.MOVIE_SIMILIAR)
    suspend fun fetchSimiliarMovies(
        @Path("movie_id") movieId: Int
    ): BaseDto<MovieDto>

    @GET(Endpoints.MOVIE_DETAIL)
    suspend fun fetchMovieDetails(
        @Path("movie_id") movieId: Int
    ): MovieDto

    @GET(Endpoints.MOVIE_CASTS)
    suspend fun fetchMovieCast(
        @Path("movie_id") movieId: Int,
    ): CastDto
}