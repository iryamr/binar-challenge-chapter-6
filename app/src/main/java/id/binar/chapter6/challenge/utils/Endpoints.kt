package id.binar.chapter6.challenge.utils

object Endpoints {

    const val NOW_PLAYING_MOVIES = "movie/now_playing"
    const val POPULAR_MOVIES = "movie/popular"
    const val TOP_RATED_MOVIES = "movie/top_rated"
    const val MOVIE_DETAIL = "movie/{movie_id}"
    const val MOVIE_CASTS = "movie/{movie_id}/credits"
    const val MOVIE_SIMILIAR = "movie/{movie_id}/similar"
}