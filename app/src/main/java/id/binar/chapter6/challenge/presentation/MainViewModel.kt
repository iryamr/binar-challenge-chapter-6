package id.binar.chapter6.challenge.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.data.sources.local.UserPreference
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    pref: UserPreference
) : ViewModel() {

//    private val _user = MutableLiveData<User>()
//    val user: LiveData<User> = _user

    val email = pref.getLoggedInEmail().asLiveData()
}