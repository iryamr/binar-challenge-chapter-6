package id.binar.chapter6.challenge.presentation.movie.movie_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.models.movie.Movie
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import id.binar.chapter6.challenge.domain.repositories.MovieRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val movieRepository: MovieRepository,
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _nowPlayingMovies = MutableLiveData<Result<List<Movie>>>(Result.Loading)
    val nowPlayingMovies: LiveData<Result<List<Movie>>> = _nowPlayingMovies

    private val _popularMovies = MutableLiveData<Result<List<Movie>>>(Result.Loading)
    val popularMovies: LiveData<Result<List<Movie>>> = _popularMovies

    private val _user = MutableLiveData<Result<User>>()
    val user: LiveData<Result<User>> = _user

    init {
        getNowPlayingMovies()
        getPopularMovies()
    }

    private fun getNowPlayingMovies() {
        viewModelScope.launch {
            val data = async { movieRepository.getNowPlayingMovies() }
            val results = data.await()
            _nowPlayingMovies.postValue(results)
        }
    }

    private fun getPopularMovies() {
        viewModelScope.launch {
            val data = async { movieRepository.getPopularMovies() }
            val results = data.await()
            _popularMovies.postValue(results)
        }
    }

    fun getLoggedInUser(email: String) {
        viewModelScope.launch {
            val data = async { authRepository.getUser(email) }
            val result = data.await()
            _user.postValue(result)
        }
    }
}