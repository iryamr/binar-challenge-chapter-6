package id.binar.chapter6.challenge.domain.models.movie

data class CastDetail(
    var id: Int,
    var movieId: Int,
    var castId: Int,
    var name: String? = null,
    var originalName: String? = null,
    var character: String? = null,
    var profilePath: String? = null
)