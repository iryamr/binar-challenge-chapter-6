package id.binar.chapter6.challenge.data.mappers

import id.binar.chapter6.challenge.data.sources.local.entities.movie.MovieEntity
import id.binar.chapter6.challenge.data.sources.remote.responses.MovieDto

//fun MovieDto.toNowPlayingMovieEntity(): MovieEntity =
//    MovieEntity(
//        id = id,
//        title = title,
//        overview = overview,
//        releaseDate = releaseDate,
//        posterPath = posterPath,
//        backdropPath = backdropPath,
//        popularity = popularity,
//        voteAverage = voteAverage,
//    )
//
//fun MovieDto.toPopularMovieEntity(): MovieEntity =
//    MovieEntity(
//        id = id,
//        title = title,
//        overview = overview,
//        releaseDate = releaseDate,
//        posterPath = posterPath,
//        backdropPath = backdropPath,
//        popularity = popularity,
//        voteAverage = voteAverage,
//    )


fun MovieDto.toEntity(): MovieEntity =
    MovieEntity(
        id = id,
        title = title,
        overview = overview,
        releaseDate = releaseDate,
        posterPath = posterPath,
        backdropPath = backdropPath,
        popularity = popularity,
        voteAverage = voteAverage,
    )

//fun MovieDto.toTopRatedMovieEntity(): TopRatedMovieEntity =
//    TopRatedMovieEntity(
//        id = id,
//        title = title,
//        posterPath = posterPath,
//        backdropPath = backdropPath,
//        voteAverage = voteAverage,
//    )

//fun MovieDto.toSimiliarMovieEntity(movieId: Int): SimiliarMovieEntity =
//    SimiliarMovieEntity(
//        id = id,
//        movieId = movieId,
//        title = title,
//        overview = overview,
//        posterPath = posterPath,
//        voteAverage = voteAverage,
//    )

//fun MovieDetailDto.toMovieDetailEntity(): MovieDetailEntity =
//    MovieDetailEntity(
//        id = id,
//        title = title,
//        posterPath = posterPath,
//        backdropPath = backdropPath,
//        releaseDate = releaseDate,
//        overview = overview,
//        voteAverage = voteAverage,
//    )