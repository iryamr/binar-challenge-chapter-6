package id.binar.chapter6.challenge.presentation

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter6.challenge.R
import id.binar.chapter6.challenge.databinding.ActivityMainBinding
import id.binar.chapter6.challenge.presentation.auth.login.LoginActivity

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkLogin()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    private fun checkLogin() {
        viewModel.email.observe(this) { email ->
            if (email.isEmpty()) {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            } else {
                val bundle = Bundle().apply { putString("email", email) }
                findNavController(R.id.main_nav_host)
                    .setGraph(R.navigation.main_navigation, bundle)
            }
        }
    }
}