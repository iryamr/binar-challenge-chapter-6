package id.binar.chapter6.challenge.presentation.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter6.challenge.data.sources.local.entities.user.UserAndProfile
import id.binar.chapter6.challenge.domain.models.Result
import id.binar.chapter6.challenge.domain.models.auth.Profile
import id.binar.chapter6.challenge.domain.models.auth.User
import id.binar.chapter6.challenge.domain.repositories.AuthRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: AuthRepository
) : ViewModel() {

    private val _profile = MutableLiveData<Result<UserAndProfile>>(Result.Loading)
    val profile: LiveData<Result<UserAndProfile>> = _profile

    private val _updateProfile = MutableLiveData<Result<Int>>(Result.Loading)
    val updateProfile: LiveData<Result<Int>> = _updateProfile

    fun getProfile(userId: Int) {
        viewModelScope.launch {
            val data = async { repository.getProfile(userId) }
            val result = data.await()
            _profile.value = result
        }
    }

    fun updateProfile(
        user: User,
        profile: Profile
    ) {
        viewModelScope.launch {
            repository.updateUser(user)
            _updateProfile.value = repository.updateProfile(profile)
        }
    }
}