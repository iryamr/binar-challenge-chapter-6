package id.binar.chapter6.challenge.domain.models.movie

data class FavoriteMovie(
    val id: Int = 0,
    val movieId: Int = 0,
    val userId: Int = 0,
    val title: String,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String,
    val overview: String,
    val popularity: Double = 0.0,
    val voteAverage: Double = 0.0,
)
