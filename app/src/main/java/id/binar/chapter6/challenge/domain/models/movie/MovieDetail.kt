package id.binar.chapter6.challenge.domain.models.movie

data class MovieDetail(
    val id: Int,
    val title: String,
    val genres: List<Genre>,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String,
    val overview: String,
    val voteAverage: Double = 0.0,
)